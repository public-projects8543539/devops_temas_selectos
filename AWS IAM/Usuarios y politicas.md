1. Crear un usuario IAM:
    - Selecciona el tipo de acceso que quieres habilitar para este usuario (por ejemplo, acceso programático, acceso a la consola de administración, o ambos).

2. Crear una política IAM para acceso restringido a algún recurso de AWS:
    - Antes de asignar los permisos, necesitas crear una política que defina el acceso específico a AWS RDS.
    - Ve a "Políticas" en IAM y haz clic en "Crear política". Puedes usar el diseñador visual para seleccionar AWS RDS y luego especificar las acciones que quieres permitir (como DescribeDBInstances, StartDBInstance, etc.).
    - Revisa y crea la política.

3. Asignar la política al usuario:
    - Regresa a la sección de "Usuarios", selecciona el usuario que creaste.
    - En la pestaña de "Permisos", haz clic en "Añadir permisos".
    - Selecciona "Asignar políticas existentes de forma directa".
    - Busca y selecciona la política que creaste para RDS.
    - Haz clic en "Siguiente: Etiquetas" (opcional) y luego en "Siguiente: Revisar".
    - Revisa los permisos y haz clic en "Signar".

4. Proporcionar las credenciales al usuario:
